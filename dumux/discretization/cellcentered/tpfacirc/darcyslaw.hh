// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup CCTpfaDiscretization
 * \brief Darcy's law for cell-centered finite volume schemes with two-point flux approximation
 */
#ifndef DUMUX_DISCRETIZATION_CC_TPFA_DARCYS_LAW_CIRC_HH
#define DUMUX_DISCRETIZATION_CC_TPFA_DARCYS_LAW_CIRC_HH

#include <limits>

#include <dumux/common/math.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/properties.hh>

#include <dumux/discretization/method.hh>
#include <dumux/flux/cctpfa/darcyslaw.hh>
#include <dumux/discretization/cellcentered/tpfacirc/computetransmissibility.hh>
#include <dumux/discretization/cellcentered/tpfacirc/fvgridgeometrycirc.hh>

namespace Dumux {

/*!
 * \ingroup CCTpfaDiscretization
 * \brief Darcy's law for cell-centered finite volume schemes with two-point flux approximation
 * \note Darcy's law is speialized for network and surface grids (i.e. if grid dim < dimWorld)
 * \tparam Scalar the scalar type for scalar physical quantities
 * \tparam FVGridGeometry the grid geometry
 * \tparam isNetwork whether we are computing on a network grid embedded in a higher world dimension
 */
template<class Scalar, class FVGridGeometry, bool isNetwork>
class CCTpfaDarcysLawCircImplementation;

/*!
 * \ingroup CCTpfaDiscretization
 * \brief Darcy's law for cell-centered finite volume schemes with two-point flux approximation
 * \note Darcy's law is speialized for network and surface grids (i.e. if grid dim < dimWorld)
 */
template <class TypeTag>
using CCTpfaDarcysLawCirc = CCTpfaDarcysLawCircImplementation<GetPropType<TypeTag, Properties::Scalar>,
                                                              GetPropType<TypeTag, Properties::GridGeometry>,
                                                              int(GetPropType<TypeTag, Properties::GridGeometry>::GridView::dimension)
                                                              < int(GetPropType<TypeTag, Properties::GridGeometry>::GridView::dimensionworld)>;

/*!
 * \ingroup CCTpfaDiscretization
 * \brief Specialization of the CCTpfaDarcysLawCirc grids where dim=dimWorld
 */
template<class ScalarType, class FVGridGeometry>
class CCTpfaDarcysLawCircImplementation<ScalarType, FVGridGeometry, /*isNetwork*/ false>
{
    using ThisType = CCTpfaDarcysLawCircImplementation<ScalarType, FVGridGeometry, /*isNetwork*/ false>;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

  public:
    //! state the scalar type of the law
    using Scalar = ScalarType;

    //! state the discretization method this implementation belongs to
    static const DiscretizationMethod discMethod = DiscretizationMethod::cctpfa;

    //! state the type for the corresponding cache
    using Cache = TpfaDarcysLawCache<ThisType, FVGridGeometry>;

    //! Compute the advective flux
    template<class Problem, class ElementVolumeVariables, class ElementFluxVarsCache>
    static Scalar flux(const Problem& problem,
                       const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolumeFace& scvf,
                       int phaseIdx,
                       const ElementFluxVarsCache& elemFluxVarsCache)
    {
        const auto& fluxVarsCache = elemFluxVarsCache[scvf];

        // Get the inside and outside volume variables
        const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
        const auto& insideVolVars = elemVolVars[insideScv];
        const auto& outsideVolVars = elemVolVars[scvf.outsideScvIdx()];

        // Obtain inside and outside pressures
        const auto pInside = insideVolVars.pressure(phaseIdx);
        const auto pOutside = outsideVolVars.pressure(phaseIdx);

        return fluxVarsCache.advectionTij()*(pInside - pOutside);
    }

    // The flux variables cache has to be bound to an element prior to flux calculations
    // During the binding, the transmissibility will be computed and stored using the method below.
    template<class Problem, class ElementVolumeVariables>
    static Scalar calculateTransmissibility(const Problem& problem,
                                            const Element& element,
                                            const FVElementGeometry& fvGeometry,
                                            const ElementVolumeVariables& elemVolVars,
                                            const SubControlVolumeFace& scvf)
    {
        Scalar tij;

        const auto insideScvIdx = scvf.insideScvIdx();
        const auto& insideScv = fvGeometry.scv(insideScvIdx);
        const auto& insideVolVars = elemVolVars[insideScvIdx];

        const Scalar ti = computeTpfaTransmissibilityCirc(scvf, insideScv, insideVolVars.permeability(), insideVolVars.extrusionFactor());

        // on the boundary (dirichlet) we only need ti
        if (scvf.boundary())
            tij = scvf.area()*ti;

        // otherwise we compute a tpfa harmonic mean
        else
        {
            const auto outsideScvIdx = scvf.outsideScvIdx();
            // as we assemble fluxes from the neighbor to our element the outside index
            // refers to the scv of our element, so we use the scv method
            const auto& outsideScv = fvGeometry.scv(outsideScvIdx);
            const auto& outsideVolVars = elemVolVars[outsideScvIdx];
            const Scalar tj = -1.0*computeTpfaTransmissibilityCirc(scvf, outsideScv, outsideVolVars.permeability(), outsideVolVars.extrusionFactor());

            // harmonic mean (check for division by zero!)
            assert(ti + tj != 0.0);
            tij = scvf.area()*(ti * tj)/(ti + tj);
        }

        return tij;
    }
};

/*!
 * \ingroup CCTpfaDiscretization
 * \brief Specialization of the CCTpfaDarcysLawCirc grids where dim < dimWorld (network/surface grids)
 */
template<class ScalarType, class FVGridGeometry>
class CCTpfaDarcysLawCircImplementation<ScalarType, FVGridGeometry, /*isNetwork*/ true>
{
    using ThisType = CCTpfaDarcysLawCircImplementation<ScalarType, FVGridGeometry, /*isNetwork*/ true>;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;

    static constexpr int dim = GridView::dimension;
    static constexpr int dimWorld = GridView::dimensionworld;

    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    //! state the scalar type of the law
    using Scalar = ScalarType;

    //! state the discretization method this implementation belongs to
    static const DiscretizationMethod discMethod = DiscretizationMethod::cctpfa;

    //! state the type for the corresponding cache
    using Cache = TpfaDarcysLawCache<ThisType, FVGridGeometry>;

    //! Compute the advective flux
    template<class Problem, class ElementVolumeVariables, class ElementFluxVarsCache>
    static Scalar flux(const Problem& problem,
                       const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolumeFace& scvf,
                       int phaseIdx,
                       const ElementFluxVarsCache& elemFluxVarsCache)
    {
        const auto& fluxVarsCache = elemFluxVarsCache[scvf];

        // Get the inside and outside volume variables
        const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
        const auto& insideVolVars = elemVolVars[insideScv];

        // Obtain inside and outside pressures
        const auto pInside = insideVolVars.pressure(phaseIdx);
        const auto pOutside = [&]()
        {
            // Dirichlet boundaries and inner faces with two neighboring elements
            if (scvf.numOutsideScvs() <= 1)
            {
                const auto& outsideVolVars = elemVolVars[scvf.outsideScvIdx()];
                return outsideVolVars.pressure(phaseIdx);
            }

            // inner faces in networks (general case)
            else
            {
                const auto& insideFluxVarsCache = elemFluxVarsCache[scvf];
                Scalar sumTi(insideFluxVarsCache.advectionTij());
                Scalar sumPTi(insideFluxVarsCache.advectionTij()*pInside);

                for (unsigned int i = 0; i < scvf.numOutsideScvs(); ++i)
                {
                    const auto outsideScvIdx = scvf.outsideScvIdx(i);
                    const auto& flippedScvf = fvGeometry.flipScvf(scvf.index(), i);
                    const auto& outsideVolVars = elemVolVars[outsideScvIdx];
                    const auto& outsideFluxVarsCache = elemFluxVarsCache[flippedScvf];
                    sumTi += outsideFluxVarsCache.advectionTij();
                    sumPTi += outsideFluxVarsCache.advectionTij()*outsideVolVars.pressure(phaseIdx);
                }
                return sumPTi/sumTi;
            }
        }();

        // return flux
        return fluxVarsCache.advectionTij()*(pInside - pOutside);
    }

    // The flux variables cache has to be bound to an element prior to flux calculations
    // During the binding, the transmissibility will be computed and stored using the method below.
    template<class Problem, class ElementVolumeVariables>
    static Scalar calculateTransmissibility(const Problem& problem,
                                            const Element& element,
                                            const FVElementGeometry& fvGeometry,
                                            const ElementVolumeVariables& elemVolVars,
                                            const SubControlVolumeFace& scvf)
    {
        Scalar tij;

        const auto insideScvIdx = scvf.insideScvIdx();
        const auto& insideScv = fvGeometry.scv(insideScvIdx);
        const auto& insideVolVars = elemVolVars[insideScvIdx];

        const Scalar ti = computeTpfaTransmissibilityCirc(scvf, insideScv, insideVolVars.permeability(), insideVolVars.extrusionFactor());

        // for the boundary (dirichlet) or at branching points we only need ti
        if (scvf.boundary() || scvf.numOutsideScvs() > 1)
            tij = scvf.area()*ti;

        // otherwise we compute a tpfa harmonic mean
        else
        {
            const auto outsideScvIdx = scvf.outsideScvIdx();
            // as we assemble fluxes from the neighbor to our element the outside index
            // refers to the scv of our element, so we use the scv method
            const auto& outsideScv = fvGeometry.scv(outsideScvIdx);
            const auto& outsideVolVars = elemVolVars[outsideScvIdx];
            const Scalar tj = computeTpfaTransmissibilityCirc(fvGeometry.flipScvf(scvf.index()), outsideScv, outsideVolVars.permeability(), outsideVolVars.extrusionFactor());

            // harmonic mean (check for division by zero!)
            assert(ti + tj != 0.0);
            tij = scvf.area()*(ti * tj)/(ti + tj);
        }

        return tij;
    }
};

} // end namespace Dumux

#endif
