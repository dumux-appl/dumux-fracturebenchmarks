// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Free function to update the finite volume geometry of the matrix domain.
 *        Overloads are available for geometries based on cell-centered schemes and
 *        for the box scheme. This differentiation is necessary as for the box scheme
 *        we need to pass a grid adapter to the update function.
 */
#include <dumux/multidomain/facet/codimonegridadapter.hh>

/*!
 * \brief Free function to update the finite volume geometry of the matrix domain
 *        in the case it uses the box scheme.
 */
template< class FVGridGeometry,
          class GridManager,
          class LowDimGridView,
          std::enable_if_t<FVGridGeometry::discMethod == Dumux::DiscretizationMethod::box, int> = 0 >
void updateMatrixFVGridGeometry(FVGridGeometry& fvGridGeometry,
                                const GridManager& gridManager,
                                const LowDimGridView& lowDimGridView)
{
    using BulkFacetGridAdapter = Dumux::CodimOneGridAdapter<typename GridManager::Embeddings>;
    BulkFacetGridAdapter facetGridAdapter(gridManager.getEmbeddings());
    fvGridGeometry.update(lowDimGridView, facetGridAdapter);
}

/*!
 * \brief Free function to update the finite volume geometry of the matrix domain
 *        in the case it uses a cell-centered scheme.
 */
 template< class FVGridGeometry,
          class GridManager,
          class LowDimGridView,
          std::enable_if_t<FVGridGeometry::discMethod != Dumux::DiscretizationMethod::box, int> = 0 >
void updateMatrixFVGridGeometry(FVGridGeometry& fvGridGeometry,
                                const GridManager& gridManager,
                                const LowDimGridView& lowDimGridView)
{
    fvGridGeometry.update();
}
