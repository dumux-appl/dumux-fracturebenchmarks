import subprocess
import sys
import os

if len(sys.argv) != 2:
    sys.stderr.write("Please provide the discretization scheme as input argument\n")
    sys.exit(1)

# parse arguments
scheme = sys.argv[1]

# check if executable exists (make otherwise)
execName = "case1_single_" + scheme
if not os.path.isfile(execName):
    subprocess.call(["make", execName])
    if not os.path.isfile(execName):
        sys.stderr.write("Could not make the target with name" + execName + "\n")
        sys.exit(1)


# run simulation for refinement 0
subprocess.call(['./' + execName,
                 "-Grid.File", "./grids/single_1k" + ("_circ" if scheme == "tpfacirc" else "") + ".msh",
                 "-IO.Refinement", "0",
                 "-IO.ClearResultFile", "true",
                 "-Matrix.OneP.Problem.Name", "onep_matrix_"  + scheme + "_0",
                 "-Matrix.Tracer.Problem.Name", "tracer_matrix_"  + scheme + "_0",
                 "-Fracture.OneP.Problem.Name", "onep_fracture_"  + scheme + "_0",
                 "-Fracture.Tracer.Problem.Name", "tracer_fracture_"  + scheme + "_0"])
# extract plot over line data
subprocess.call(['pvpython', "makeplotoverlinedata.py", scheme, "0"])

# run simulation for refinement 1
subprocess.call(['./' + execName,
                 "-Grid.File", "./grids/single_10k" + ("_circ" if scheme == "tpfacirc" else "") + ".msh",
                 "-IO.Refinement", "1",
                 "-IO.ClearResultFile", "false",
                 "-Matrix.OneP.Problem.Name", "onep_matrix_"  + scheme + "_1",
                 "-Matrix.Tracer.Problem.Name", "tracer_matrix_"  + scheme + "_1",
                 "-Fracture.OneP.Problem.Name", "onep_fracture_"  + scheme + "_1",
                 "-Fracture.Tracer.Problem.Name", "tracer_fracture_"  + scheme + "_1"])
# extract plot over line data
subprocess.call(['pvpython', "makeplotoverlinedata.py", scheme, "1"])

# run simulation for refinement 2
subprocess.call(['./' + execName,
                 "-Grid.File", "./grids/single_100k" + ("_circ" if scheme == "tpfacirc" else "") + ".msh",
                 "-IO.Refinement", "2",
                 "-IO.ClearResultFile", "false",
                 "-Matrix.OneP.Problem.Name", "onep_matrix_"  + scheme + "_2",
                 "-Matrix.Tracer.Problem.Name", "tracer_matrix_"  + scheme + "_2",
                 "-Fracture.OneP.Problem.Name", "onep_fracture_"  + scheme + "_2",
                 "-Fracture.Tracer.Problem.Name", "tracer_fracture_"  + scheme + "_2"])
# extract plot over line data
subprocess.call(['pvpython', "makeplotoverlinedata.py", scheme, "2"])
