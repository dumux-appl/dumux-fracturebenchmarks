// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Main file of the benchmark case 1 - single fracture
 */
#include <config.h>

#include <iostream>
#include <fstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/istl/matrixmarket.hh>

#include "problem_1p_matrix.hh"
#include "problem_1p_fracture.hh"

#include "problem_tracer_matrix.hh"
#include "problem_tracer_fracture.hh"

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/assembly/diffmethod.hh>
#include <dumux/discretization/method.hh>
#include <dumux/discretization/evalgradients.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/linear/matrixconverter.hh>

#include <dumux/multidomain/newtonsolver.hh>
#include <dumux/multidomain/fvassembler.hh>
#include <dumux/multidomain/traits.hh>
#include <dumux/multidomain/fvgridgeometry.hh>
#include <dumux/multidomain/fvproblem.hh>
#include <dumux/multidomain/fvgridvariables.hh>
#include <dumux/multidomain/io/vtkoutputmodule.hh>

#include <dumux/multidomain/facet/gridmanager.hh>
#include <dumux/multidomain/facet/couplingmapper.hh>
#include <dumux/multidomain/facet/couplingmanager.hh>

// function to compute volume-fluxes per scvf
#include <cases/transport/common/computevolumefluxes.hh>

// function to write out the results file as specified in point 3 of sub-section 5.4
#include <cases/transport/common/writeresultsfile.hh>

// function to update the matrix fv grid geometry
#include <common/updatefvgridgeometry.hh>

// Get the actual type tags used from CMakeLists.txt
using MatrixOnePTypeTag = Dumux::Properties::TTag::ONEPMATRIXTYPETAG;
using FractureOnePTypeTag = Dumux::Properties::TTag::ONEPFRACTURETYPETAG;
using MatrixTracerTypeTag = Dumux::Properties::TTag::TRACERMATRIXTYPETAG;
using FractureTracerTypeTag = Dumux::Properties::TTag::TRACERFRACTURETYPETAG;

// obtain/define some types to be used below in the property definitions and in main
template<class MatrixTypeTag, class FractureTypeTag>
class TestTraits
{
    using MatrixFVGridGeometry = Dumux::GetPropType<MatrixTypeTag, Dumux::Properties::GridGeometry>;
    using FractureFVGridGeometry = Dumux::GetPropType<FractureTypeTag, Dumux::Properties::GridGeometry>;
public:
    using MDTraits = Dumux::MultiDomainTraits<MatrixTypeTag, FractureTypeTag>;
    using CouplingMapper = Dumux::FacetCouplingMapper<MatrixFVGridGeometry, FractureFVGridGeometry>;
    using CouplingManager = Dumux::FacetCouplingManager<MDTraits, CouplingMapper>;
};

// set the coupling manager property in the sub-problems
namespace Dumux {
namespace Properties {

using OnePTraits = TestTraits<MatrixOnePTypeTag, FractureOnePTypeTag>;
using TracerTraits = TestTraits<MatrixTracerTypeTag, FractureTracerTypeTag>;

template<class TypeTag> struct CouplingManager<TypeTag, MatrixOnePTypeTag> { using type = typename OnePTraits::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, FractureOnePTypeTag> { using type = typename OnePTraits::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, MatrixTracerTypeTag> { using type = typename TracerTraits::CouplingManager; };
template<class TypeTag> struct CouplingManager<TypeTag, FractureTracerTypeTag> { using type = typename TracerTraits::CouplingManager; };

} // end namespace Properties
} // end namespace Dumux

// writes out the output data over time
template<class MatrixFluxVariables,
         class Problem,
         class GridVariables,
         class SolutionVector>
void writeOutput(std::ofstream& file,
                 const double t,
                 const Problem& problem,
                 const GridVariables& gridVariables,
                 const SolutionVector& x)
{
    const auto& matrixProblem = problem[Dune::index_constant<0>()];
    const auto& matrixGridVars = gridVariables[Dune::index_constant<0>()];
    const auto& fractureProblem = problem[Dune::index_constant<1>()];
    const auto& fractureGridVars = gridVariables[Dune::index_constant<1>()];

    double massBottomLayer = 0.0;
    double fluxAcrossOutlet = 0.0;
    for (const auto& element : elements(matrixProblem.gridGeometry().gridView()))
    {
        if (element.geometry().center()[2] < 10.0)
        {
            auto fvGeometry = localView(matrixProblem.gridGeometry());
            auto elemVolVars = localView(matrixGridVars.curGridVolVars());
            auto elemFluxVarsCache = localView(matrixGridVars.gridFluxVarsCache());

            fvGeometry.bind(element);
            elemVolVars.bind(element, fvGeometry, x[Dune::index_constant<0>()]);
            elemFluxVarsCache.bind(element, fvGeometry, elemVolVars);

            for (const auto& scv : scvs(fvGeometry))
            {
                const auto& vv = elemVolVars[scv];
                massBottomLayer += vv.porosity()*vv.moleFraction(0, 0)*scv.volume();
            }

            for (const auto& scvf : scvfs(fvGeometry))
                if (matrixProblem.isOnOutlet(scvf.ipGlobal()))
                    fluxAcrossOutlet += matrixProblem.neumann(element, fvGeometry, elemVolVars, elemFluxVarsCache, scvf)*scvf.area();
        }
    }

    double massFracture = 0.0;
    for (const auto& element : elements(fractureProblem.gridGeometry().gridView()))
    {
        auto fvGeometry = localView(fractureProblem.gridGeometry());
        auto elemVolVars = localView(fractureGridVars.curGridVolVars());

        fvGeometry.bindElement(element);
        elemVolVars.bindElement(element, fvGeometry, x[Dune::index_constant<1>()]);

        for (const auto& scv : scvs(fvGeometry))
        {
            const auto& vv = elemVolVars[scv];
            massFracture += vv.extrusionFactor()*vv.porosity()*vv.moleFraction(0, 0)*scv.volume();
        }
    }

    file << t << "," << massBottomLayer << "," << massFracture << "," << fluxAcrossOutlet << std::endl;
}

// main program
int main(int argc, char** argv) try
{
    using namespace Dumux;

    //////////////////////////////////////////////////////
    //////////////////////////////////////////////////////

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // initialize parameter tree
    Parameters::init(argc, argv);

    // the multidomain traits and some indices
    using OnePTraits = TestTraits<MatrixOnePTypeTag, FractureOnePTypeTag>;
    using OnePMDTraits = typename OnePTraits::MDTraits;
    constexpr auto onePMatrixId = OnePMDTraits::template SubDomain<0>::Index{};
    constexpr auto onePFractureId = OnePMDTraits::template SubDomain<1>::Index{};

    using GridManager = FacetCouplingGridManager<typename OnePMDTraits::template SubDomain<onePMatrixId>::Grid,
                                                 typename OnePMDTraits::template SubDomain<onePFractureId>::Grid>;
    GridManager gridManager;
    gridManager.init();
    gridManager.loadBalance();

    // we compute on the leaf grid views
    const auto& matrixGridView = gridManager.template grid<0>().leafGridView();
    const auto& fractureGridView = gridManager.template grid<1>().leafGridView();

    // containers to store the volume fluxes (will be passed to tracer problem)
    std::vector< std::vector<double> > matrixVolumeFluxes;
    std::vector< std::vector<double> > fractureVolumeFluxes;

    // create the finite volume grid geometries
    using FVGridGeometry = MultiDomainFVGridGeometry<OnePMDTraits>;
    FVGridGeometry fvGridGeometry(std::make_tuple(matrixGridView, fractureGridView));
    updateMatrixFVGridGeometry(fvGridGeometry[onePMatrixId], gridManager, fractureGridView);
    fvGridGeometry[onePFractureId].update();

    // check if matrix uses the box scheme
    const bool matrixUsesBox = FVGridGeometry::Type<onePMatrixId>::discMethod == DiscretizationMethod::box;

    // the coupling mapper
    auto couplingMapper = std::make_shared<typename OnePTraits::CouplingMapper>();
    couplingMapper->update(fvGridGeometry[onePMatrixId], fvGridGeometry[onePFractureId], gridManager.getEmbeddings());

    ////////////////////////////////////////////////////////////
    // run stationary, simgle-phase problem on this grid
    ////////////////////////////////////////////////////////////

    // put the following code in brackets such that memory is released afterwards
    {
        // instantiate coupling manager
        using CouplingManager = typename OnePTraits::CouplingManager;
        auto couplingManager = std::make_shared<CouplingManager>();

        // the problems (boundary conditions)
        MultiDomainFVProblem<OnePMDTraits> problem;

        using MatrixProblem = MultiDomainFVProblem<OnePMDTraits>::template Type<onePMatrixId>;
        using FractureProblem = MultiDomainFVProblem<OnePMDTraits>::template Type<onePFractureId>;

        auto matrixSpatialParams = std::make_shared<typename MatrixProblem::SpatialParams>(fvGridGeometry.get(onePMatrixId));
        auto fractureSpatialParams = std::make_shared<typename FractureProblem::SpatialParams>(fvGridGeometry.get(onePFractureId));

        problem.set(std::make_shared<MatrixProblem>(fvGridGeometry.get(onePMatrixId), matrixSpatialParams, couplingManager, "Matrix.OneP"), onePMatrixId);
        problem.set(std::make_shared<FractureProblem>(fvGridGeometry.get(onePFractureId), fractureSpatialParams, couplingManager, "Fracture.OneP"), onePFractureId);

        // the solution vector
        typename OnePMDTraits::SolutionVector x;
        x[onePMatrixId].resize(fvGridGeometry[onePMatrixId].numDofs());
        x[onePFractureId].resize(fvGridGeometry[onePFractureId].numDofs());
        problem[onePMatrixId].applyInitialSolution(x[onePMatrixId]);
        problem[onePFractureId].applyInitialSolution(x[onePFractureId]);

        // initialize the coupling manager
        couplingManager->init(problem.get(onePMatrixId), problem.get(onePFractureId), couplingMapper, x);

        // the grid variables
        using GridVariables = MultiDomainFVGridVariables<OnePMDTraits>;
        GridVariables gridVars(fvGridGeometry.getTuple(), problem.getTuple());
        gridVars.init(x);

        // intialize the vtk output module
        using VtkOutputModule = MultiDomainVtkOutputModule<OnePMDTraits>;
        using MatrixOnePOutputModule = VtkOutputModule::Type<onePMatrixId>;
        using FractureOnePOutputModule = VtkOutputModule::Type<onePFractureId>;

        VtkOutputModule vtkWriter;
        const auto dm = matrixUsesBox ? Dune::VTK::nonconforming : Dune::VTK::conforming;
        vtkWriter.set(std::make_shared<MatrixOnePOutputModule>(gridVars[onePMatrixId], x[onePMatrixId], problem[onePMatrixId].name(), "Matrix", dm), onePMatrixId);
        vtkWriter.set(std::make_shared<FractureOnePOutputModule>(gridVars[onePFractureId], x[onePFractureId], problem[onePFractureId].name(), "Fracture"), onePFractureId);
        vtkWriter.initDefaultOutputFields();
        vtkWriter.write(0.0);

        // the assembler
        using Assembler = MultiDomainFVAssembler<OnePMDTraits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
        auto assembler = std::make_shared<Assembler>( problem.getTuple(),
                                                      fvGridGeometry.getTuple(),
                                                      gridVars.getTuple(),
                                                      couplingManager);

        // the linear solver
        using LinearSolver = UMFPackBackend;
        auto linearSolver = std::make_shared<LinearSolver>();

        // the non-linear solver
        using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
        auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);

        // linearize & solve
        newtonSolver->solve(x);

        // update grid variables for output
        gridVars.update(x);

        // write vtk output
        vtkWriter.write(1.0);

        // write out results.csv file
        std::ofstream results( "results.csv", (getParam<bool>("IO.ClearResultFile") ? std::ofstream::out : std::ofstream::app) );
        writeResultsFile(results, fvGridGeometry[onePMatrixId], fvGridGeometry[onePFractureId], assembler->jacobian());

        // compute the volume fluxes and store them in the arrays
        std::cout << "Computing volume fluxes...";
        using MatrixFluxVariables = GetPropType<MatrixOnePTypeTag, Properties::FluxVariables>;
        using FractureFluxVariables = GetPropType<FractureOnePTypeTag, Properties::FluxVariables>;
        computeVolumeFluxes<MatrixFluxVariables>(matrixVolumeFluxes, *couplingManager, *assembler, problem[onePMatrixId],
                                                 fvGridGeometry[onePMatrixId], gridVars[onePMatrixId], x[onePMatrixId], onePMatrixId);
        computeVolumeFluxes<FractureFluxVariables>(fractureVolumeFluxes, *couplingManager, *assembler, problem[onePFractureId],
                                                   fvGridGeometry[onePFractureId], gridVars[onePFractureId], x[onePFractureId], onePFractureId);
        std::cout << "done" << std::endl;
    }

    ////////////////////////////////////////////////////////////////////////////
    // run instationary tracer problem on this grid with the precomputed fluxes
    ////////////////////////////////////////////////////////////////////////////

    // obtain the traits class
    using TracerTraits = TestTraits<MatrixTracerTypeTag, FractureTracerTypeTag>;
    using TracerMDTraits = typename TracerTraits::MDTraits;
    constexpr auto tracerMatrixId = OnePMDTraits::template SubDomain<0>::Index{};
    constexpr auto tracerFractureId = OnePMDTraits::template SubDomain<1>::Index{};

    // instantiate coupling manager
    using CouplingManager = typename TracerTraits::CouplingManager;
    auto couplingManager = std::make_shared<CouplingManager>();

    // instantiate the tracer problems reusing the fv grid geometries
    MultiDomainFVProblem<TracerMDTraits> problem;

    using MatrixProblem = MultiDomainFVProblem<TracerMDTraits>::template Type<tracerMatrixId>;
    using FractureProblem = MultiDomainFVProblem<TracerMDTraits>::template Type<tracerFractureId>;

    auto matrixSpatialParams = std::make_shared<typename MatrixProblem::SpatialParams>(fvGridGeometry.get(tracerMatrixId), std::move(matrixVolumeFluxes));
    auto fractureSpatialParams = std::make_shared<typename FractureProblem::SpatialParams>(fvGridGeometry.get(tracerFractureId), std::move(fractureVolumeFluxes));

    problem.set(std::make_shared<MatrixProblem>(fvGridGeometry.get(tracerMatrixId), matrixSpatialParams, couplingManager, "Matrix.Tracer"), tracerMatrixId);
    problem.set(std::make_shared<FractureProblem>(fvGridGeometry.get(tracerFractureId), fractureSpatialParams, couplingManager, "Fracture.Tracer"), tracerFractureId);

    // the solution vectors and system matrix
    typename TracerMDTraits::SolutionVector x, xOld;
    x[tracerMatrixId].resize(fvGridGeometry[tracerMatrixId].numDofs());
    x[tracerFractureId].resize(fvGridGeometry[tracerFractureId].numDofs());
    problem.applyInitialSolution(x);
    xOld = x;

    // initialize the coupling manager
    couplingManager->init(problem.get(tracerMatrixId), problem.get(tracerFractureId), couplingMapper, x);

    // the grid variables
    using GridVariables = MultiDomainFVGridVariables<TracerMDTraits>;
    GridVariables gridVars(fvGridGeometry.getTuple(), problem.getTuple());
    gridVars.init(x);

    // intialize the vtk output module
    using VtkOutputModule = MultiDomainVtkOutputModule<TracerMDTraits>;
    using MatrixTracerOutputModule = VtkOutputModule::Type<onePMatrixId>;
    using FractureTracerOutputModule = VtkOutputModule::Type<onePFractureId>;

    VtkOutputModule vtkWriter;
    const auto dm = matrixUsesBox ?  Dune::VTK::nonconforming : Dune::VTK::conforming;
    vtkWriter.set(std::make_shared<MatrixTracerOutputModule>(gridVars[onePMatrixId], x[onePMatrixId], problem[onePMatrixId].name(), "Matrix", dm), onePMatrixId);
    vtkWriter.set(std::make_shared<FractureTracerOutputModule>(gridVars[onePFractureId], x[onePFractureId], problem[onePFractureId].name(), "Fracture"), onePFractureId);
    vtkWriter.initDefaultOutputFields();
    vtkWriter.write(0.0);

    //! get some time loop parameters
    const auto tEnd = getParam<double>("TimeLoop.TEnd");
    auto dt = getParam<double>("TimeLoop.Dt");

    //! instantiate time loop
    auto timeLoop = std::make_shared<TimeLoop<double>>(0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(dt);

    // the assembler
    using Assembler = MultiDomainFVAssembler<TracerMDTraits, CouplingManager, DiffMethod::numeric, /*implicit?*/true>;
    auto assembler = std::make_shared<Assembler>( problem.getTuple(),
                                                  fvGridGeometry.getTuple(),
                                                  gridVars.getTuple(),
                                                  couplingManager, timeLoop);

    // the linear solver
    using LinearSolver = UMFPackBackend;
    auto linearSolver = std::make_shared<LinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::MultiDomainNewtonSolver<Assembler, LinearSolver, CouplingManager>;
    auto newtonSolver = std::make_shared<NewtonSolver>(assembler, linearSolver, couplingManager);

    //! start the time loop
    std::ofstream dataOverTime("dot_refinement_" + getParam<std::string>("IO.Refinement") + ".csv", std::ios::out);
    timeLoop->start(); do
    {
        // set previous solution for storage evaluations
        assembler->setPreviousSolution(xOld);

        // solve the system
        newtonSolver->solve(x);

        // make the new solution the old solution
        xOld = x;
        gridVars.advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // write vtk output on check points
        vtkWriter.write(timeLoop->time());

        // append output data to data over time
        using MatrixFluxVars = GetPropType<MatrixTracerTypeTag, Properties::FluxVariables>;
        writeOutput<MatrixFluxVars>(dataOverTime, timeLoop->time(), problem, gridVars, x);

        // report statistics of this time step
        timeLoop->reportTimeStep();
    } while (!timeLoop->finished());

    timeLoop->finalize();

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/false);

    // print used/unused parameters
    Parameters::print();

    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::DGFException & e)
{
    std::cerr << "DGF exception thrown (" << e <<
                 "). Most likely, the DGF file name is wrong "
                 "or the DGF file is corrupted, "
                 "e.g. missing hash at end of file or wrong number (dimensions) of entries."
                 << " ---> Abort!" << std::endl;
    return 2;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
