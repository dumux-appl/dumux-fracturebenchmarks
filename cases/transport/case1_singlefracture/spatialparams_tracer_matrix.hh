// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Spatial parameters class for the transport problem
 *        of the benchmark case 1 - single fracture
 */
#ifndef DUMUX_FRACTUREBENCHMARKS_CASE1_SINGLEFRACTURE_FRACTURE_TRANSPORT_SPATIALPARAMS_MATRIX_HH
#define DUMUX_FRACTUREBENCHMARKS_CASE1_SINGLEFRACTURE_FRACTURE_TRANSPORT_SPATIALPARAMS_MATRIX_HH

#include <dumux/porousmediumflow/properties.hh>
#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux {

/*!
 * \ingroup TracerTests
 * \brief Definition of the spatial parameters for the tracer problem
 */
template<class FVGridGeometry, class Scalar>
class CaseOneSpatialParamsTracerMatrix
: public FVSpatialParamsOneP<FVGridGeometry, Scalar,
                             CaseOneSpatialParamsTracerMatrix<FVGridGeometry, Scalar>>
{
    using ThisType = CaseOneSpatialParamsTracerMatrix<FVGridGeometry, Scalar>;
    using ParentType = FVSpatialParamsOneP<FVGridGeometry, Scalar, ThisType>;

    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    static constexpr bool isBox = FVGridGeometry::discMethod == DiscretizationMethod::box;
public:

    CaseOneSpatialParamsTracerMatrix(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                                     std::vector< std::vector<Scalar> >&& volumeFluxes)
    : ParentType(fvGridGeometry)
    , volumeFlux_(std::move(volumeFluxes))
    {}

    /*!
     * \brief Define the porosity \f$\mathrm{[-]}\f$.
     *
     * \param globalPos The global position
     */
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return globalPos[2] > 10.0 ? 0.2 : 0.25; }

    //! Fluid properties that are spatial params in the tracer model
    //! They can possible vary with space but are usually constants
    //! fluid density
    Scalar fluidDensity(const Element &element,
                        const SubControlVolume& scv) const
    { return 1.0; }

    //! fluid molar mass
    Scalar fluidMolarMass(const Element &element,
                          const SubControlVolume& scv) const
    { return 1.0; }

    Scalar fluidMolarMass(const GlobalPosition &globalPos) const
    { return 1.0; }

    //! velocity field
    template<class ElementVolumeVariables>
    Scalar volumeFlux(const Element &element,
                      const FVElementGeometry& fvGeometry,
                      const ElementVolumeVariables& elemVolVars,
                      const SubControlVolumeFace& scvf) const
    {
        return isBox ? volumeFlux_[this->gridGeometry().elementMapper().index(element)][scvf.index()]
                     : volumeFlux_[scvf.index()][0];
    }

private:
    std::vector< std::vector<Scalar> > volumeFlux_;
};

} // end namespace Dumux

#endif
