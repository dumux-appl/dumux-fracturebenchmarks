import csv
import sys
import subprocess
import numpy as np

try:
    from paraview.simple import *
except ImportError:
    print("`paraview.simple` not found. Make sure using pvpython instead of python.")

if len(sys.argv) != 3:
    sys.stderr.write("Please provide the discretization scheme refinement level as input arguments\n")
    sys.exit(1)

# parse arguments
scheme = sys.argv[1]
refinement = sys.argv[2]

if scheme != "mpfa" and scheme != "tpfa" and scheme != "box" and scheme != "tpfacirc":
    sys.stderr.write("Invalid discretization scheme provided\n")
    sys.exit(1)

# index of arc length and head in the csv files created by paraview
arcIdxOneP = 3
headIdxOneP = 0
arcIdxTracer = 5
concIdxTracer = 0

# hydraulic head through the matrix (point 5)
fileName = "case1_single_onep_matrix_" + scheme + "_" + refinement + "-00001.vtu"
vtkFile = XMLUnstructuredGridReader(FileName=fileName)
SetActiveSource(vtkFile)

# apply and configure PlotOverLine filter
plotOverLine = PlotOverLine(Source="High Resolution Line Source")
plotOverLine.Source.Resolution = 2000
plotOverLine.Source.Point1 = [0, 100, 100]
plotOverLine.Source.Point2 = [100, 0, 0]

# write output to csv file
csvFile = 'pol_1.csv'
writer = CreateWriter(csvFile, plotOverLine)
writer.UpdatePipeline()

# obtain data
plot1Data = np.loadtxt(csvFile, delimiter=',', skiprows=1)
subprocess.call(['rm', csvFile])

# concentration through the matrix at final time (point 6)
fileName = "case1_single_tracer_matrix_" + scheme + "_" + refinement + "-00100.vtu"
vtkFile = XMLUnstructuredGridReader(FileName=fileName)
SetActiveSource(vtkFile)

# apply and configure PlotOverLine filter
plotOverLine = PlotOverLine(Source="High Resolution Line Source")
plotOverLine.Source.Resolution = 2000
plotOverLine.Source.Point1 = [0, 100, 100]
plotOverLine.Source.Point2 = [100, 0, 0]

# write output to csv file
csvFile = 'pol_2.csv'
writer = CreateWriter(csvFile, plotOverLine)
writer.UpdatePipeline()

# add data
plot2Data = np.loadtxt(csvFile, delimiter=',', skiprows=1)
subprocess.call(['rm', csvFile])

# concentration through the fracture at final time (point 7)
fileName = "case1_single_tracer_fracture_" + scheme + "_" + refinement + "-00100.vtu"
vtkFile = XMLUnstructuredGridReader(FileName=fileName)
SetActiveSource(vtkFile)

# apply and configure PlotOverLine filter
plotOverLine = PlotOverLine(Source="High Resolution Line Source")
plotOverLine.Source.Resolution = 2000
plotOverLine.Source.Point1 = [0, 100, 80]
plotOverLine.Source.Point2 = [100, 0, 20]

# write output to csv file
csvFile = 'pol_3.csv'
writer = CreateWriter(csvFile, plotOverLine)
writer.UpdatePipeline()

# add data
plot3Data = np.loadtxt(csvFile, delimiter=',', skiprows=1)
subprocess.call(['rm', csvFile])

# write everything into combined csv file
resultFile = open("dol_refinement_" + refinement + ".csv", "w")

for i in range(0, len(plot1Data)):
    resultFile.write(str(plot1Data[i][arcIdxOneP]) + ",")
    resultFile.write(str(plot1Data[i][headIdxOneP]) + ",")
    resultFile.write(str(plot2Data[i][arcIdxTracer]) + ",")
    resultFile.write(str(plot2Data[i][concIdxTracer]) + ",")
    resultFile.write(str(plot3Data[i][arcIdxTracer]) + ",")
    resultFile.write(str(plot3Data[i][concIdxTracer]) + "\n")

resultFile.close()
