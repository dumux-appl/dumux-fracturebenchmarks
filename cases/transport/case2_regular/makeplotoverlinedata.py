import csv
import sys
import subprocess
import numpy as np

try:
    from paraview.simple import *
except ImportError:
    print("`paraview.simple` not found. Make sure using pvpython instead of python.")

if len(sys.argv) != 4:
    sys.stderr.write("Please provide the discretization scheme, refinement level & conductivity index as input arguments\n")
    sys.exit(1)

# parse arguments
scheme = sys.argv[1]
refinement = sys.argv[2]
cond = sys.argv[3]

if scheme != "mpfa" and scheme != "tpfa" and scheme != "box" and scheme != "tpfacirc":
    sys.stderr.write("Invalid discretization scheme provided\n")
    sys.exit(1)

if cond != "0" and cond != "1":
    sys.stderr.write("Conductivity index must be either 0 or 1\n")
    sys.exit(1)

# index of arc length and head in the csv files created by paraview
arcIdxOneP = 3
headIdxOneP = 0
arcIdxTracer = 5
concIdxTracer = 0

# hydraulic head through the matrix (point 5)
fileName = "case2_regular_onep_matrix_" + scheme + "_cond_" + cond + "_ref_" + refinement + "-00001.vtu"
vtkFile = XMLUnstructuredGridReader(FileName=fileName)
SetActiveSource(vtkFile)

# apply and configure PlotOverLine filter
plotOverLine = PlotOverLine(Source="High Resolution Line Source")
plotOverLine.Source.Resolution = 2000
plotOverLine.Source.Point1 = [0, 0, 0]
plotOverLine.Source.Point2 = [1, 1, 1]

# write output to csv file
csvFile = 'pol.csv'
writer = CreateWriter(csvFile, plotOverLine)
writer.UpdatePipeline()

# obtain data
plotData = np.loadtxt(csvFile, delimiter=',', skiprows=1)
subprocess.call(['rm', csvFile])

# write head into reordered csv file
resultFile = open("dol_cond_" + cond + "_refinement_" + refinement + ".csv", "w")

for i in range(0, len(plotData)):
    resultFile.write(str(plotData[i][arcIdxOneP]) + ",")
    resultFile.write(str(plotData[i][headIdxOneP]) + "\n")

resultFile.close()
