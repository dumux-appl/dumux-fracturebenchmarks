import subprocess
import sys
import os

if len(sys.argv) != 3:
    sys.stderr.write("Please provide the discretization scheme and the conductivity index as input arguments\n")
    sys.exit(1)

# parse arguments
scheme = sys.argv[1]
cond = sys.argv[2]

# deduce fracture permeability parameter from given conductivity index
if cond == "0":
    fractureK = "1e4"
    fracturePhi = "0.9"
elif cond == "1":
    fractureK = "1e-4"
    fracturePhi = "0.01"
else:
    sys.stderr.write("Conductivity index must be either 0 or 1\n")
    sys.exit(1)

# check if executable exists (make otherwise)
execName = "case2_regular_" + scheme
if not os.path.isfile(execName):
    subprocess.call(["make", execName])
    if not os.path.isfile(execName):
        sys.stderr.write("Could not make the target with name" + execName + "\n")
        sys.exit(1)

# run simulation for refinement 0
subprocess.call(['./' + execName,
                 "-Grid.File", "./grids/regular_1k" + ("_circ" if scheme == "tpfacirc" else "") + ".msh",
                 "-CMPercentage", "1e-7",
                 "-IO.Refinement", "0",
                 "-IO.ClearResultFile", "true",
                 "-Fracture.SpatialParams.Permeability", fractureK,
                 "-Fracture.SpatialParams.Porosity", fracturePhi,
                 "-Matrix.OneP.Problem.Name", "onep_matrix_"  + scheme + "_cond_" + cond + "_ref_0",
                 "-Matrix.Tracer.Problem.Name", "tracer_matrix_"  + scheme + "_cond_" + cond + "_ref_0",
                 "-Fracture.OneP.Problem.Name", "onep_fracture_"  + scheme + "_cond_" + cond + "_ref_0",
                 "-Fracture.Tracer.Problem.Name", "tracer_fracture_"  + scheme + "_cond_" + cond + "_ref_0"])
# extract plot over line data
subprocess.call(['pvpython', "makeplotoverlinedata.py", scheme, "0", cond])

# run simulation for refinement 1
subprocess.call(['./' + execName,
                 "-Grid.File", "./grids/regular_4k" + ("_circ" if scheme == "tpfacirc" else "") + ".msh",
                 "-IO.Refinement", "1",
                 "-IO.ClearResultFile", "false",
                 "-Fracture.SpatialParams.Permeability", fractureK,
                 "-Fracture.SpatialParams.Porosity", fracturePhi,
                 "-Matrix.OneP.Problem.Name", "onep_matrix_"  + scheme + "_cond_" + cond + "_ref_1",
                 "-Matrix.Tracer.Problem.Name", "tracer_matrix_"  + scheme + "_cond_" + cond + "_ref_1",
                 "-Fracture.OneP.Problem.Name", "onep_fracture_"  + scheme + "_cond_" + cond + "_ref_1",
                 "-Fracture.Tracer.Problem.Name", "tracer_fracture_"  + scheme + "_cond_" + cond + "_ref_1"])
# extract plot over line data
subprocess.call(['pvpython', "makeplotoverlinedata.py", scheme, "1", cond])

# run simulation for refinement 2
subprocess.call(['./' + execName,
                 "-Grid.File", "./grids/regular_44k" + ("_circ" if scheme == "tpfacirc" else "") + ".msh",
                 "-IO.Refinement", "2",
                 "-IO.ClearResultFile", "false",
                 "-Fracture.SpatialParams.Permeability", fractureK,
                 "-Fracture.SpatialParams.Porosity", fracturePhi,
                 "-Matrix.OneP.Problem.Name", "onep_matrix_"  + scheme + "_cond_" + cond + "_ref_2",
                 "-Matrix.Tracer.Problem.Name", "tracer_matrix_"  + scheme + "_cond_" + cond + "_ref_2",
                 "-Fracture.OneP.Problem.Name", "onep_fracture_"  + scheme + "_cond_" + cond + "_ref_2",
                 "-Fracture.Tracer.Problem.Name", "tracer_fracture_"  + scheme + "_cond_" + cond + "_ref_2"])
# extract plot over line data
subprocess.call(['pvpython', "makeplotoverlinedata.py", scheme, "2", cond])
