// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Spatial parameters class for the matrix domain in the flow
 *        problem of the benchmark case 4- field case
 */
#ifndef DUMUX_FRACTUREBENCHMARKS_CASE4_FIELDCASE_FRACTURE_FLOW_SPATIALPARAMS_MATRIX_HH
#define DUMUX_FRACTUREBENCHMARKS_CASE4_FIELDCASE_FRACTURE_FLOW_SPATIALPARAMS_MATRIX_HH

#include <dumux/material/spatialparams/fv1p.hh>

namespace Dumux {

template< class FVGridGeometry, class Scalar >
class CaseFourSpatialParamsOnePMatrix
: public FVSpatialParamsOneP< FVGridGeometry, Scalar, CaseFourSpatialParamsOnePMatrix<FVGridGeometry, Scalar> >
{
    using ThisType = CaseFourSpatialParamsOnePMatrix< FVGridGeometry, Scalar >;
    using ParentType = FVSpatialParamsOneP< FVGridGeometry, Scalar, ThisType >;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    //! export the type used for permeabilities
    using PermeabilityType = Scalar;

    //! the constructor
    CaseFourSpatialParamsOnePMatrix(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {}

    //! Function for defining the (intrinsic) permeability \f$[m^2]\f$.
    PermeabilityType permeabilityAtPos(const GlobalPosition& globalPos) const
    { return 1.0; }

    //! Return the porosity
    Scalar porosityAtPos(const GlobalPosition& globalPos) const
    { return 0.2; }
};

} // end namespace Dumux

#endif
