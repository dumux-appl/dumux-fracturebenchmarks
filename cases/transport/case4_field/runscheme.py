import subprocess
import sys
import os

if len(sys.argv) != 2:
    sys.stderr.write("Please provide the discretization scheme as input argument\n")
    sys.exit(1)

# parse arguments
scheme = sys.argv[1]

# check if executable exists (make otherwise)
execName = "case4_field_" + scheme
if not os.path.isfile(execName):
    subprocess.call(["make", execName])
    if not os.path.isfile(execName):
        sys.stderr.write("Could not make the target with name" + execName + "\n")
        sys.exit(1)

# run simulation
subprocess.call(['./' + execName,
                 "-Grid.File", "./grids/field.msh",
                 "-CMPercentage", "1e-3",
                 "-Matrix.OneP.Problem.Name", "onep_matrix_"  + scheme,
                 "-Matrix.Tracer.Problem.Name", "tracer_matrix_"  + scheme,
                 "-Fracture.OneP.Problem.Name", "onep_fracture_"  + scheme,
                 "-Fracture.Tracer.Problem.Name", "tracer_fracture_"  + scheme])
# extract plot over line data
subprocess.call(['pvpython', "makeplotoverlinedata.py", scheme])
