// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Modified finite volume grid geometry traits with
 *        an increased stencil size, necessary for the grid
 *        used in this test case.
 */
#ifndef DUMUX_CASE4_CC_MPFA_FV_GRID_GEOMETRY_TRAITS_HH
#define DUMUX_CASE4_CC_MPFA_FV_GRID_GEOMETRY_TRAITS_HH

#include <dumux/discretization/cellcentered/mpfa/fvgridgeometrytraits.hh>

namespace Dumux {

/*!
 * \brief Modified finite volume grid geometry traits with
 *        an increased stencil size, necessary for the grid
 *        used in this test case.
 */
template<class GV, class NI, class PIV, class SIV>
struct CaseFourFractureMpfaFVGridGeometryTraits
: public CCMpfaFVGridGeometryTraits<GV, NI, PIV, SIV>
{
    //! Use increased stencil size
    static constexpr int maxElementStencilSize = 50;
};

} // end namespace Dumux

#endif
