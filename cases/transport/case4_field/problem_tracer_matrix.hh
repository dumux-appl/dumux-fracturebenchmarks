// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
 /*!
  * \file
  * \brief Problem class for the tracer transport problem in the fracture domain
  *        of the benchmark case 4 - field case
  */
#ifndef DUMUX_FRACTUREBENCHMARKS_CASE4_FIELDCASE_MATRIX_TRANSPORT_PROBLEM_HH
#define DUMUX_FRACTUREBENCHMARKS_CASE4_FIELDCASE_MATRIX_TRANSPORT_PROBLEM_HH

#include <dune/alugrid/grid.hh>

#include <dumux/multidomain/facet/cellcentered/mpfa/properties.hh>
#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>
#include <dumux/multidomain/facet/box/properties.hh>

#include <dumux/porousmediumflow/tracer/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <dumux/material/fluidsystems/base.hh>

#include <cases/transport/common/tracerfluidsystem.hh>
#include <cases/transport/common/tracermodeltraits.hh>

#include "spatialparams_tracer_matrix.hh"

namespace Dumux {

template <class TypeTag>
class CaseFourTracerMatrixProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct TracerMatrix { using InheritsFrom = std::tuple<Tracer>; };
struct TracerMatrixTpfa { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, TracerMatrix>; };
struct TracerMatrixTpfaCirc { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, TracerMatrix>; };
struct TracerMatrixMpfa { using InheritsFrom = std::tuple<CCMpfaFacetCouplingModel, TracerMatrix>; };
struct TracerMatrixBox { using InheritsFrom = std::tuple<BoxFacetCouplingModel, TracerMatrix>; };
} // end namespace TTag

//! Overwrite the advection type property
template<class TypeTag>
struct AdvectionType<TypeTag, TTag::TracerMatrixTpfa> { using type = StationaryVelocityField<GetPropType<TypeTag, Properties::Scalar>>; };
template<class TypeTag>
struct AdvectionType<TypeTag, TTag::TracerMatrixTpfaCirc> { using type = StationaryVelocityField<GetPropType<TypeTag, Properties::Scalar>>; };
template<class TypeTag>
struct AdvectionType<TypeTag, TTag::TracerMatrixMpfa> { using type = StationaryVelocityField<GetPropType<TypeTag, Properties::Scalar>>; };
template<class TypeTag>
struct AdvectionType<TypeTag, TTag::TracerMatrixBox> { using type = StationaryVelocityField<GetPropType<TypeTag, Properties::Scalar>>; };

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TracerMatrix> { using type = Dune::ALUGrid<3, 3, Dune::simplex, Dune::nonconforming>; };

// === TPFACirc specific properties ===
#if HAVE_DUNE_MMESH
template<class TypeTag>
struct Grid<TypeTag, TTag::TracerMatrixTpfaCirc> { using type = Dune::MovingMesh<3>; };
#endif

// Set the finite volume geometry to the one using circumcenters
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::TracerMatrixTpfaCirc>
{
private:
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = GetPropType<TypeTag, Properties::GridView>;
public:
    using type = CCTpfaFVGridGeometry<GridView, enableCache, CCTpfaCircDefaultGridGeometryTraits<GridView>>;
};
// ====================================

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::TracerMatrix> { using type = CaseFourTracerMatrixProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TracerMatrix>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = CaseFourSpatialParamsTracerMatrix<FVGridGeometry, Scalar>;
};

//! set the model traits (with disabled diffusion)
template<class TypeTag>
struct ModelTraits<TypeTag, TTag::TracerMatrix>
{
private:
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
public:
    using type = BenchmarkTracerModelTraits<FluidSystem::numComponents, getPropValue<TypeTag, Properties::UseMoles>()>;
};

// use the modified tracer fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TracerMatrix>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
public:
    using type = FluidSystems::BenchmarkTracerFluidSystem<FVGridGeometry, Scalar>;
};
} // end namespace Properties


template <class TypeTag>
class CaseFourTracerMatrixProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename FVGridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    using typename ParentType::SpatialParams;

    CaseFourTracerMatrixProblem(std::shared_ptr<const FVGridGeometry> fvGridGeom,
                                std::shared_ptr<SpatialParams> spatialParams,
                                std::shared_ptr<CouplingManager> couplingManager,
                                const std::string& paramGroup = "")
    : ParentType(fvGridGeom, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManager)
    {
        problemName_  =  getParamFromGroup<std::string>(this->paramGroup(), "Vtk.OutputName")
                         + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
    }

    //! The problem name.
    const std::string& name() const
    { return problemName_; }

    //! Specify the type of boundary conditions at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    //! Specify the type of boundary conditions at a given position
    BoundaryTypes interiorBoundaryTypes(const Element& element,
                                        const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    //! Specify the initial conditions at a given position
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    //! specify Dirichlet boundary conditions at a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return initialAtPos(globalPos); }

    //! Evaluate the Neumann BCS for a boundary segment
    template<class ElementVolumeVariables, class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        if (isOnInlet(scvf.ipGlobal()))
            return NumEqVector(-1.0);
        else if (isOnOutlet(scvf.ipGlobal()))
        {
            const auto flux = this->spatialParams().volumeFlux(element, fvGeometry, elemVolVars, scvf)/scvf.area();
            if (flux < -1e-6)
                DUNE_THROW(Dune::InvalidStateException, "Outflux expected at outlet boundary!");

            const auto& insideVolVars = elemVolVars[fvGeometry.scv(scvf.insideScvIdx())];
            const auto tracerFlux = insideVolVars.massFraction(/*phaseIdx*/0, /*compIdx*/0)*flux;
            return NumEqVector(tracerFlux);
        }
        else
            return NumEqVector(0.0);
    }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

    //! returns true if position is on inlet
    bool isOnInlet(const GlobalPosition& globalPos) const
    { return isOnInlet1(globalPos) || isOnInlet2(globalPos); }

    //! returns true if position is on inlet boundary segment 1
    bool isOnInlet1(const GlobalPosition& globalPos) const
    {
        return globalPos[0] > -500.0 && globalPos[0] < -200.0
               && globalPos[1] > 1500.0 - 1e-6
               && globalPos[2] > 300.0 && globalPos[2] < 500.0;
    }

    //! returns true if position is on inlet boundary segment 2
    bool isOnInlet2(const GlobalPosition& globalPos) const
    {
        return globalPos[0] < -500.0 + 1e-6
               && globalPos[1] > 1200.0 && globalPos[1] < 1500.0
               && globalPos[2] > 300.0 && globalPos[2] < 500.0;
    }

    //! returns true if position is on outlet
    bool isOnOutlet(const GlobalPosition& globalPos) const
    { return isOnOutlet1(globalPos) || isOnOutlet2(globalPos); }

    //! returns true if position is on outlet boundary segment 1
    bool isOnOutlet1(const GlobalPosition& globalPos) const
    {
        return globalPos[0] < -500.0 + 1e-6
               && globalPos[1] > 100.0 && globalPos[1] < 400.0
               && globalPos[2] > -100.0 && globalPos[2] < 100.0;
    }

    //! returns true if position is on outlet boundary segment 2
    bool isOnOutlet2(const GlobalPosition& globalPos) const
    {
        return globalPos[0] > 350.0 - 1e-6
               && globalPos[1] > 100.0 && globalPos[1] < 400.0
               && globalPos[2] > -100.0 && globalPos[2] < 100.0;
    }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    std::string problemName_;
};

} //end namespace Dumux

#endif
