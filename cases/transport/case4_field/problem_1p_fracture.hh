// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Problem class for the flow problem in the fracture domain of the
 *        benchmark case 4 - field case
 */
#ifndef DUMUX_FRACTUREBENCHMARKS_CASE4_FIELDCASE_FRACTURE_FLOW_PROBLEM_HH
#define DUMUX_FRACTUREBENCHMARKS_CASE4_FIELDCASE_FRACTURE_FLOW_PROBLEM_HH

#include <dune/foamgrid/foamgrid.hh>

#if HAVE_DUNE_MMESH
#include <dune/mmesh/mmesh.hh>
#endif

#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>
#include <dumux/discretization/box.hh>
#include <dumux/discretization/cellcentered/tpfacirc/darcyslaw.hh>

#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

#include "spatialparams_1p_fracture.hh"
#include "fvgridgeometrytraits.hh"

namespace Dumux {

// problem class forward declaration
template <class TypeTag>
class CaseFourFractureFlowProblem;

namespace Properties {

// define type tag node for this problem
namespace TTag {
struct OnePFracture { using InheritsFrom = std::tuple<OneP>; };
struct OnePFractureTpfa { using InheritsFrom = std::tuple<OnePFracture, CCTpfaModel>; };
struct OnePFractureTpfaCirc { using InheritsFrom = std::tuple<OnePFracture, CCTpfaModel>; };
struct OnePFractureMpfa { using InheritsFrom = std::tuple<OnePFracture, CCMpfaModel>; };
struct OnePFractureBox { using InheritsFrom = std::tuple<OnePFracture, BoxModel>; };
}

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePFracture> { using type = Dune::FoamGrid<2, 3>; };

// === TPFACirc specific properties ===
#if HAVE_DUNE_MMESH
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePFractureTpfaCirc> { using type = Dune::MovingMesh<3>::InterfaceGrid; };
#endif
// Select own darcys law using the circumcenters
template<class TypeTag>
struct AdvectionType<TypeTag, TTag::OnePFractureTpfaCirc> { using type = CCTpfaDarcysLawCirc<TypeTag>; };

//! Set the finite volume geometry to the one using circumcenters
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::OnePFractureTpfaCirc>
{
private:
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = GetPropType<TypeTag, Properties::GridView>;
public:
    using type = CCTpfaFVGridGeometry<GridView, enableCache, CCTpfaCircDefaultGridGeometryTraits<GridView>>;
};
// ====================================

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePFracture> { using type = CaseFourFractureFlowProblem<TypeTag>; };

// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePFracture>
{
    using type = CaseFourSpatialParamsOnePFracture< GetPropType<TypeTag, Properties::GridGeometry>,
                                                    GetPropType<TypeTag, Properties::Scalar> >;
};

//! Set grid finite volume geometry with modified traits class (for mpfa)
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::OnePFractureMpfa>
{
private:
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using PrimaryIV = GetPropType<TypeTag, Properties::PrimaryInteractionVolume>;
    using SecondaryIV = GetPropType<TypeTag, Properties::SecondaryInteractionVolume>;
    using NodalIndexSet = GetPropType<TypeTag, Properties::DualGridNodalIndexSet>;
    using Traits = CaseFourFractureMpfaFVGridGeometryTraits<GridView, NodalIndexSet, PrimaryIV, SecondaryIV>;
public:
    using type = CCMpfaFVGridGeometry<GridView, Traits, getPropValue<TypeTag, Properties::EnableGridGeometryCache>()>;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePFracture>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = FluidSystems::OnePLiquid< Scalar, Components::Constant</*id*/0, Scalar> >;
};

// solution-independent advection
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::OnePFracture> { static constexpr bool value = false; };
}

template<class TypeTag>
class CaseFourFractureFlowProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

public:
    //! The constructor
    CaseFourFractureFlowProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                                std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                                std::shared_ptr<CouplingManager> couplingManager,
                                const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManager)
    {
        problemName_ = getParamFromGroup<std::string>(this->paramGroup(), "Vtk.OutputName") + "_" +
                       getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
    }

    //! Return the problem name
    const std::string& name() const
    { return problemName_; }

    //! Return the (constant) temperature in the domain
    Scalar temperature() const
    { return 283.15; }

    //! Return the initial conditions at a given position
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    //! Return the Neumann boundary conditions at a given position
    NumEqVector neumannAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    //! Evaluates the source term for all phases within a given sub-control-volume.
    template<class ElementVolumeVariables>
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // forward to solution independent, fully-implicit specific interface
        auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);
        source /= scv.volume()*elemVolVars[scv].extrusionFactor();
        return source;
    }

    //! Set the aperture as extrusion factor.
    Scalar extrusionFactorAtPos(const GlobalPosition& globalPos) const
    { return 1e-2; }

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    //! Return reference to the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    std::string problemName_;
};

} //end namespace Dumux

#endif
