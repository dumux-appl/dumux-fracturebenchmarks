import os
import sys
import subprocess

if len(sys.argv) != 2:
    sys.stderr.write("Please provide the target results folder as input argument\n")
    sys.exit(1)

# parse arguments
resultPath = sys.argv[1]

# call run script for tpfa
subprocess.call(['python', "runscheme.py", "tpfa"])

# check whether scheme's sub-folder exists already (if not create it)
tpfaPath = resultPath + "TPFA"
if not os.path.exists(tpfaPath):
    subprocess.call(['mkdir', tpfaPath])

# copy output files to results folder
subprocess.call(['cp', 'dot_refinement_0.csv', tpfaPath])
subprocess.call(['cp', 'dot_refinement_1.csv', tpfaPath])
subprocess.call(['cp', 'dol_line_0_refinement_0.csv', tpfaPath])
subprocess.call(['cp', 'dol_line_0_refinement_1.csv', tpfaPath])
subprocess.call(['cp', 'dol_line_1_refinement_0.csv', tpfaPath])
subprocess.call(['cp', 'dol_line_1_refinement_1.csv', tpfaPath])
subprocess.call(['cp', "results.csv", tpfaPath])


# call run script for tpfacirc
subprocess.call(['python', "runscheme.py", "tpfacirc"])

# check whether scheme's sub-folder exists already (if not create it)
tpfaCircPath = resultPath + "TPFACIRC"
if not os.path.exists(tpfaCircPath):
    subprocess.call(['mkdir', tpfaCircPath])

# copy output files to results folder
subprocess.call(['cp', 'dot_refinement_0.csv', tpfaCircPath])
subprocess.call(['cp', 'dot_refinement_1.csv', tpfaCircPath])
subprocess.call(['cp', 'dol_line_0_refinement_0.csv', tpfaCircPath])
subprocess.call(['cp', 'dol_line_0_refinement_1.csv', tpfaCircPath])
subprocess.call(['cp', 'dol_line_1_refinement_0.csv', tpfaCircPath])
subprocess.call(['cp', 'dol_line_1_refinement_1.csv', tpfaCircPath])
subprocess.call(['cp', "results.csv", tpfaCircPath])


# call run script for box
subprocess.call(['python', "runscheme.py", "box"])

# check whether scheme's sub-folder exists already (if not create it)
boxPath = resultPath + "BOX"
if not os.path.exists(boxPath):
    subprocess.call(['mkdir', boxPath])

# copy output files to results folder
subprocess.call(['cp', 'dot_refinement_0.csv', boxPath])
subprocess.call(['cp', 'dot_refinement_1.csv', boxPath])
subprocess.call(['cp', 'dol_line_0_refinement_0.csv', boxPath])
subprocess.call(['cp', 'dol_line_0_refinement_1.csv', boxPath])
subprocess.call(['cp', 'dol_line_1_refinement_0.csv', boxPath])
subprocess.call(['cp', 'dol_line_1_refinement_1.csv', boxPath])
subprocess.call(['cp', "results.csv", boxPath])


# call run script for mpfa
subprocess.call(['python', "runscheme.py", "mpfa"])

# check whether scheme's sub-folder exists already (if not create it)
mpfaPath = resultPath + "MPFA"
if not os.path.exists(mpfaPath):
    subprocess.call(['mkdir', mpfaPath])

# copy output files to results folder
subprocess.call(['cp', 'dot_refinement_0.csv', mpfaPath])
subprocess.call(['cp', 'dot_refinement_1.csv', mpfaPath])
subprocess.call(['cp', 'dol_line_0_refinement_0.csv', mpfaPath])
subprocess.call(['cp', 'dol_line_0_refinement_1.csv', mpfaPath])
subprocess.call(['cp', 'dol_line_1_refinement_0.csv', mpfaPath])
subprocess.call(['cp', 'dol_line_1_refinement_1.csv', mpfaPath])
subprocess.call(['cp', "results.csv", mpfaPath])
