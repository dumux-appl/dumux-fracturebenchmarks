// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Problem class for the flow problem in the porous matrix of the
 *        benchmark case 3 - small features
 */
#ifndef DUMUX_FRACTUREBENCHMARKS_CASE3_SMALLFEATURES_MATRIX_FLOW_PROBLEM_HH
#define DUMUX_FRACTUREBENCHMARKS_CASE3_SMALLFEATURES_MATRIX_FLOW_PROBLEM_HH

#include <dune/alugrid/grid.hh>

#if HAVE_DUNE_MMESH
#include <dune/mmesh/mmesh.hh>
#include <dumux/multidomain/facet/mmeshgridmanager.hh>
#endif

#include <dumux/multidomain/facet/cellcentered/tpfa/properties.hh>
#include <dumux/multidomain/facet/cellcentered/mpfa/properties.hh>
#include <dumux/multidomain/facet/box/properties.hh>
#include <dumux/multidomain/facet/cellcentered/tpfacirc/darcyslaw.hh>

#include <dumux/porousmediumflow/1p/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/components/constant.hh>

#include "spatialparams_1p_matrix.hh"

namespace Dumux {

// problem class forward declaration
template <class TypeTag>
class CaseThreeMatrixFlowProblem;

namespace Properties {

// define type tag node for this problem
namespace TTag {
struct OnePMatrix { using InheritsFrom = std::tuple<OneP>; };
struct OnePMatrixTpfa { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, OnePMatrix>; };
struct OnePMatrixTpfaCirc { using InheritsFrom = std::tuple<CCTpfaFacetCouplingModel, OnePMatrix>; };
struct OnePMatrixMpfa { using InheritsFrom = std::tuple<CCMpfaFacetCouplingModel, OnePMatrix>; };
struct OnePMatrixBox { using InheritsFrom = std::tuple<BoxFacetCouplingModel, OnePMatrix>; };
}

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePMatrix> { using type = Dune::ALUGrid<3, 3, Dune::simplex, Dune::nonconforming>; };

// === TPFACirc specific properties ===
#if HAVE_DUNE_MMESH
template<class TypeTag>
struct Grid<TypeTag, TTag::OnePMatrixTpfaCirc> { using type = Dune::MovingMesh<3>; };
#endif
// Select own darcys law using the circumcenters
template<class TypeTag>
struct AdvectionType<TypeTag, TTag::OnePMatrixTpfaCirc> { using type = CCTpfaFacetCouplingDarcysLawCirc<TypeTag>; };

// Set the finite volume geometry to the one using circumcenters
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::OnePMatrixTpfaCirc>
{
private:
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = GetPropType<TypeTag, Properties::GridView>;
public:
    using type = CCTpfaFVGridGeometry<GridView, enableCache, CCTpfaCircDefaultGridGeometryTraits<GridView>>;
};
// ====================================

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::OnePMatrix> { using type = CaseThreeMatrixFlowProblem<TypeTag>; };

// set the spatial params
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::OnePMatrix>
{
    using type = CaseThreeSpatialParamsOnePMatrix< GetPropType<TypeTag, Properties::GridGeometry>,
                                                   GetPropType<TypeTag, Properties::Scalar> >;
};

// the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::OnePMatrix>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = FluidSystems::OnePLiquid< Scalar, Components::Constant</*id*/0, Scalar> >;
};

// solution-independent advection
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::OnePMatrix> { static constexpr bool value = false; };
}

template<class TypeTag>
class CaseThreeMatrixFlowProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using GridView = typename FVGridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;

public:
    //! The constructor
    CaseThreeMatrixFlowProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                               std::shared_ptr<typename ParentType::SpatialParams> spatialParams,
                               std::shared_ptr<CouplingManager> couplingManager,
                               const std::string& paramGroup = "")
    : ParentType(fvGridGeometry, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManager)
    {
        problemName_ = getParamFromGroup<std::string>(this->paramGroup(), "Vtk.OutputName") + "_" +
                       getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
    }

    //! Return the problem name
    const std::string& name() const
    { return problemName_; }

    //! Return the (constant) temperature in the domain
    Scalar temperature() const
    { return 283.15; }

    //! Evaluate the source term at a given position
    PrimaryVariables sourceAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    //! Evaluate the initial conditions at a given position
    PrimaryVariables initialAtPos(const GlobalPosition& globalPos) const
    { return PrimaryVariables(0.0); }

    //! Evaluate the Neumann boundary conditions
    template<class ElementVolumeVariables, class ElemFluxVarsCache>
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const ElemFluxVarsCache& elemFluxVarsCache,
                        const SubControlVolumeFace& scvf) const
    {
        if (isOnInlet(scvf.ipGlobal()))
            return PrimaryVariables(-1.0);
        else if (isOnOutlet(scvf.ipGlobal()) && FVGridGeometry::discMethod == DiscretizationMethod::box)
        {
            // modify element solution to carry outlet head
            auto elemSol = elementSolution(element, elemVolVars, fvGeometry);
            for (const auto& curScvf : scvfs(fvGeometry))
            {
                if (curScvf.boundary())
                {
                    const bool curOnOutlet = isOnOutlet(curScvf.ipGlobal());
                    if (curOnOutlet)
                    {
                        const auto diriValues = dirichletAtPos(curScvf.ipGlobal());
                        const auto& insideScv = fvGeometry.scv(curScvf.insideScvIdx());
                        elemSol[insideScv.localDofIndex()][0] = diriValues[0];
                    }
                }
            }

            // evaluate gradients using this element solution
            const auto gradHead = evalGradients(element,
                                                element.geometry(),
                                                fvGeometry.gridGeometry(),
                                                elemSol,
                                                scvf.ipGlobal())[0];

            // compute the flux
            const auto& volVars = elemVolVars[scvf.insideScvIdx()];
            Scalar flux = gradHead * scvf.unitOuterNormal();
            flux *= -1.0 *volVars.permeability();
            flux *= volVars.density()*volVars.mobility();
            return PrimaryVariables(flux);
        }
        else
            return PrimaryVariables(0.0);
    }

    //! Specifies the type of interior boundary condition at a given position
    BoundaryTypes interiorBoundaryTypes(const Element& element, const SubControlVolumeFace& scvf) const
    {
        BoundaryTypes values;

        // since this case uses highly permeable fractures, use Dirichlet BCs
        values.setAllDirichlet();

        return values;
    }

    //! Specifies the type of boundary condition at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition& globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();

        //! For cc methods, use Dirichlet at outlet
        if (FVGridGeometry::discMethod != DiscretizationMethod::box)
            if (isOnOutlet(globalPos))
                values.setAllDirichlet();

        return values;
    }

    //! Evaluate the Dirichlet BCS at a given position
    PrimaryVariables dirichletAtPos(const GlobalPosition& globalPos) const
    { return initialAtPos(globalPos); }

    //! returns true if position is on inlet
    bool isOnInlet(const GlobalPosition& globalPos) const
    { return globalPos[1] < 1e-6 && globalPos[2] > 1.0/3.0 && globalPos[2] < 2.0/3.0; }

    //! returns true if position is on outlet
    bool isOnOutlet(const GlobalPosition& globalPos) const
    { return isOnOutlet1(globalPos) || isOnOutlet2(globalPos); }

    //! returns true if position is on outlet boundary segment 1
    bool isOnOutlet1(const GlobalPosition& globalPos) const
    { return globalPos[1] > 2.25 - 1e-6 && globalPos[2] < 1.0/3.0; }

    //! returns true if position is on outlet boundary segment 2
    bool isOnOutlet2(const GlobalPosition& globalPos) const
    { return globalPos[1] > 2.25 - 1e-6 && globalPos[2] > 2.0/3.0; }

    //! Return reference to the coupling manager
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    std::string problemName_;
};

} //end namespace Dumux

#endif
