// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
 /*!
  * \file
  * \brief Problem class for the tracer transport problem in the fracture domain
  *        of the benchmark case 3 - small features
  */
#ifndef DUMUX_FRACTUREBENCHMARKS_CASE3_SMALLFEATURES_FRACTURE_TRANSPORT_PROBLEM_HH
#define DUMUX_FRACTUREBENCHMARKS_CASE3_SMALLFEATURES_FRACTURE_TRANSPORT_PROBLEM_HH

#include <dune/foamgrid/foamgrid.hh>

#if HAVE_DUNE_MMESH
#include <dune/mmesh/mmesh.hh>
#endif

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>

#include <dumux/porousmediumflow/tracer/model.hh>
#include <dumux/porousmediumflow/problem.hh>

#include <dumux/material/fluidsystems/base.hh>

#include <cases/transport/common/tracerfluidsystem.hh>
#include <cases/transport/common/tracermodeltraits.hh>
#include "spatialparams_tracer_fracture.hh"

namespace Dumux {

template <class TypeTag>
class CaseThreeTracerFractureProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct TracerFracture { using InheritsFrom = std::tuple<Tracer>; };
struct TracerFractureTpfa { using InheritsFrom = std::tuple<TracerFracture, CCTpfaModel>; };
struct TracerFractureTpfaCirc { using InheritsFrom = std::tuple<TracerFracture, CCTpfaModel>; };
struct TracerFractureMpfa { using InheritsFrom = std::tuple<TracerFracture, CCMpfaModel>; };
struct TracerFractureBox { using InheritsFrom = std::tuple<TracerFracture, BoxModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TracerFracture> { using type = Dune::FoamGrid<2, 3>; };

// === TPFACirc specific properties ===
#if HAVE_DUNE_MMESH
template<class TypeTag>
struct Grid<TypeTag, TTag::TracerFractureTpfaCirc> { using type = Dune::MovingMesh<3>::InterfaceGrid; };
#endif

// Set the finite volume geometry to the one using circumcenters
template<class TypeTag>
struct GridGeometry<TypeTag, TTag::TracerFractureTpfaCirc>
{
private:
    static constexpr bool enableCache = getPropValue<TypeTag, Properties::EnableGridGeometryCache>();
    using GridView = GetPropType<TypeTag, Properties::GridView>;
public:
    using type = CCTpfaFVGridGeometry<GridView, enableCache, CCTpfaCircDefaultGridGeometryTraits<GridView>>;
};
// ====================================

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::TracerFracture> { using type = CaseThreeTracerFractureProblem<TypeTag>; };

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TracerFracture>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = CaseThreeSpatialParamsTracerFracture<FVGridGeometry, Scalar>;
};

// use the modified tracer fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TracerFracture>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
public:
    using type = FluidSystems::BenchmarkTracerFluidSystem<FVGridGeometry, Scalar>;
};

//! set the model traits (with disabled diffusion)
template<class TypeTag>
struct ModelTraits<TypeTag, TTag::TracerFracture>
{
private:
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
public:
    using type = BenchmarkTracerModelTraits<FluidSystem::numComponents, getPropValue<TypeTag, Properties::UseMoles>()>;
};

// solution-independent advection
template<class TypeTag>
struct SolutionDependentAdvection<TypeTag, TTag::TracerFracture> { static constexpr bool value = false; };
} // end namespace Properties


/*!
 * \ingroup MultiDomain
 * \ingroup TracerModel
 *
 * \brief The problem for the bulk domain of the tracer facet coupling test
 */
template <class TypeTag>
class CaseThreeTracerFractureProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using SubControlVolume = typename FVGridGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVGridGeometry::SubControlVolumeFace;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using CouplingManager = GetPropType<TypeTag, Properties::CouplingManager>;

    using GridView = typename FVGridGeometry::GridView;
    using Element = typename FVGridGeometry::GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    using typename ParentType::SpatialParams;

    CaseThreeTracerFractureProblem(std::shared_ptr<const FVGridGeometry> fvGridGeom,
                                   std::shared_ptr<SpatialParams> spatialParams,
                                   std::shared_ptr<CouplingManager> couplingManager,
                                   const std::string& paramGroup = "")
    : ParentType(fvGridGeom, spatialParams, paramGroup)
    , couplingManagerPtr_(couplingManager)
    {
        problemName_  =  getParamFromGroup<std::string>(this->paramGroup(), "Vtk.OutputName")
                         + "_" + getParamFromGroup<std::string>(this->paramGroup(), "Problem.Name");
    }

    //! The problem name.
    const std::string& name() const
    { return problemName_; }

    //! Specify the type of boundary conditions at a given position
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        values.setAllNeumann();
        return values;
    }

    //! Specify the initial conditions at a given position
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    { return PrimaryVariables(0.0); }

    //! Evaluate the source term at a given position
    template<class ElementVolumeVariables>
    NumEqVector source(const Element& element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume& scv) const
    {
        // evaluate sources from bulk domain
        auto source = couplingManagerPtr_->evalSourcesFromBulk(element, fvGeometry, elemVolVars, scv);
        source /= scv.volume()*elemVolVars[scv].extrusionFactor();
        return source;
    }

    //! Evaluate the Neumann BCS on a boundary segment
    NumEqVector neumannAtPos(const GlobalPosition& globalPos) const
    { return NumEqVector(0.0); }

    //! Set the aperture as extrusion factor.
    Scalar extrusionFactorAtPos(const GlobalPosition& globalPos) const
    { return 1e-2; }

    //! returns reference to the coupling manager.
    const CouplingManager& couplingManager() const
    { return *couplingManagerPtr_; }

private:
    std::shared_ptr<CouplingManager> couplingManagerPtr_;
    std::string problemName_;
};

} //end namespace Dumux

#endif
